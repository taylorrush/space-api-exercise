import React, { Component } from 'react'
import classNames from 'class-names'
import Spinner from '@atlaskit/spinner'
import Tabs from '@atlaskit/tabs'
import DynamicTable from '@atlaskit/dynamic-table'

import './App.css'

class App extends Component {
  state = {
      assets: [],
      entries: [],
      spaces: [],
      selectedSpaceIndex: 0,
      selectedSpaceId: '',
      isLoading: true,
      hasError: false
  }

  async componentDidMount() {
    const spaceIdFromUrl = this.props.history.location.pathname.split('/')[1]
    try {
      const spaces = await this.getSpaces()
      // Default to the first space
      let selectedSpaceIndex = 0

      if (spaceIdFromUrl) {
        const matchingSpaceIndex = spaces.items.findIndex(space => {
          return space.sys.id === spaceIdFromUrl
        })

        // If there is not a matching space then continue with the default space
        if (matchingSpaceIndex !== -1) {
          selectedSpaceIndex = matchingSpaceIndex
        }
      }

      const selectedSpaceId = spaces.items[selectedSpaceIndex].sys.id
      this.props.history.push(`/${selectedSpaceId}`)
      const entriesAndAssets = await this.getEntriesAndAssetsWithUserName(spaces.items[this.state.selectedSpaceIndex])
      this.setState({
        ...entriesAndAssets,
        spaces: spaces.items,
        selectedSpaceIndex,
        selectedSpaceId,
        isLoading: false
      })
    }

    catch (error) {
      this.setState({
        isLoading: false,
        hasError: true
      })
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevState.selectedSpaceIndex !== this.state.selectedSpaceIndex) {
      this.props.history.push(`/${this.state.selectedSpaceId}`)
      const entriesAndAssets = await this.getEntriesAndAssetsWithUserName(this.state.spaces[this.state.selectedSpaceIndex])
      this.setState({
        ...entriesAndAssets
      })
    }
  }

  getSpaces = async () => {
    const spacesResponse = await fetch('/space')
    const spaces = await spacesResponse.json()

    if (spacesResponse.status !== 200) {
      throw Error(spaces.message)
    }

    return spaces
  }

  getSpaceById = async (spaceId) => {
    const spaceResponse = await fetch(`/space/${spaceId}`)
    const space = await spaceResponse.json()

    if (spaceResponse.status !== 200) {
      throw Error(space.message)
    }

    return space
  }

  getEntriesBySpaceId = async (spaceId) => {
    const entriesResponse = await fetch(`/space/${spaceId}/entries`)
    const entries = await entriesResponse.json()

    if (entriesResponse.status !== 200) {
      throw Error(entries.message)
    }

    return entries
  }

  getAssetsBySpaceId = async (spaceId) => {
    const assetsResponse = await fetch(`/space/${spaceId}/assets`)
    const assets = await assetsResponse.json()

    if (assetsResponse.status !== 200) {
      throw Error(assets.message)
    }

    return assets
  }

  getUserById = async (userId) => {
    const userResponse = await fetch(`/users/${userId}`)
    const user = await userResponse.json()

    if (userResponse.status !== 200) {
      throw Error(user.message)
    }

    return user
  }

  getUserNamesAndFormattedDate = async (entryOrAsset) => {
    let createdByUser = ''
    let updatedByUser = ''

    try {
      const userData = await this.getUserById(entryOrAsset.sys.createdBy)
      createdByUser = userData.fields.name
    }

    catch {
      createdByUser = 'User Unavailable'
    }

    try {
      const userData = await this.getUserById(entryOrAsset.sys.updatedBy)
      updatedByUser = userData.fields.name
    }

    catch {
      updatedByUser = 'User Unavailable'
    }

    const updatedDate = new Date(entryOrAsset.sys.updatedAt)
    const formattedUpdatedDate = `${updatedDate.getMonth() + 1}-${updatedDate.getDate()}-${updatedDate.getFullYear()}`

    return {
      createdByUser,
      updatedByUser,
      formattedUpdatedDate
    }
  }

  getEntriesAndAssetsWithUserName = async (selectedSpace) => {
    try {
      const entriesResponse = await this.getEntriesBySpaceId(selectedSpace.sys.id)
      const assetsResponse = await this.getAssetsBySpaceId(selectedSpace.sys.id)

      const entries = await Promise.all(entriesResponse.items.map(async entry => {
        const {
          createdByUser,
          updatedByUser,
          formattedUpdatedDate
        } = await this.getUserNamesAndFormattedDate(entry)

        return Promise.resolve({
          title: entry.fields.title,
          summary: entry.fields.summary,
          createdBy: createdByUser,
          updatedBy: updatedByUser,
          lastUpdated: formattedUpdatedDate
        })
      }))

      const assets = await Promise.all(assetsResponse.items.map(async asset => {
        const {
          createdByUser,
          updatedByUser,
          formattedUpdatedDate
        } = await this.getUserNamesAndFormattedDate(asset)

        return Promise.resolve({
          title: asset.fields.title,
          contentType: asset.fields.contentType,
          fileName: asset.fields.fileName,
          createdBy: createdByUser,
          updatedBy: updatedByUser,
          lastUpdated: formattedUpdatedDate
        })
      }))

      return {
        entries,
        assets
      }
    }

    catch {
      return {
        entries: [],
        assets: []
      }
    }
  }

  render() {
    // Create a tab group for the user to swap between spaces
    // Inside of the spaces tab group will be another tab group
    // that houses the entries and assets for the selected space
    const spaceTabs = this.state.spaces.map(space => {
      const entryTableHead = {
        cells: [
          {content: 'Title', isSortable: true, key: 'Title'},
          {content: 'Summary', isSortable: true, key: 'Summary'},
          {content: 'Created By', isSortable: true, key: 'Created By'},
          {content: 'Updated By', isSortable: true, key: 'Updated By'},
          {content: 'Last Updated', isSortable: true, key: 'Last Updated'}
        ]
      }

      const EntryTabContent = (
        <DynamicTable
          head={entryTableHead}
          rows={this.state.entries.map(entry => {
            return {
              cells: [
                {content: entry.title, key: entry.title},
                {content: entry.summary, key: entry.summary},
                {content: entry.createdBy, key: entry.createdBy},
                {content: entry.updatedBy, key: entry.updatedBy},
                {content: entry.lastUpdated, key: entry.lastUpdated}
              ]
            }
          })}
          rowsPerPage={10}
          defaultPage={1}
          defaultSortKey="term"
          defaultSortOrder="ASC"
          sortKey="term"
        />
      )

      const assetTableHead = {
        cells: [
          {content: 'Title', isSortable: true, key: 'Title'},
          {content: 'Content Type', isSortable: true, key: 'Content Type'},
          {content: 'File Name', isSortable: true, key: 'File Name'},
          {content: 'Created By', isSortable: true, key: 'Created By'},
          {content: 'Updated By', isSortable: true, key: 'Updated By'},
          {content: 'Last Updated', isSortable: true, key: 'Last Updated'}
        ]
      }

      const AssetTabContent = (
        <DynamicTable
          head={assetTableHead}
          rows={this.state.assets.map(asset => {
            return {
              cells: [
                {content: asset.title, key: asset.title},
                {content: asset.contentType, key: asset.contentType},
                {content: asset.fileName, key: asset.fileName},
                {content: asset.createdBy, key: asset.createdBy},
                {content: asset.updatedBy, key: asset.updatedBy},
                {content: asset.lastUpdated, key: asset.lastUpdated}
              ]
            }
          })}
          rowsPerPage={10}
          defaultPage={1}
          defaultSortKey="term"
          defaultSortOrder="ASC"
          sortKey="term"
        />
      )

      const entriesAndAssetsTabs = [
        {
          label: 'Entries',
          content: EntryTabContent
        },
        {
          label: 'Assets',
          content: AssetTabContent
        }
      ]

      const SpaceTabContent = (
        <React.Fragment>
          <h1>{space.fields.title}</h1>
          <p>{space.fields.description}</p>
          <div className='entriesAndAssetsTabs'>
            <Tabs tabs={entriesAndAssetsTabs} />
          </div>
        </React.Fragment>
      )

      const spaceTab = {
        label: space.fields.title,
        content: SpaceTabContent
      }

      return spaceTab
    })

    const loaderClasses = classNames({
      'loader': true,
      'is-loading': this.state.isLoading
    })

    return (
      <div className="App">
        {/* Show the user a loading screen while we wait for the data to be returned */}
        <div className={loaderClasses}>
          <Spinner
            isCompleting={!this.state.isLoading}
            size='xlarge'
          />
        </div>
        {/* If the data is not successfully returned. alert the user */}
        { this.state.hasError &&
          <div className='errorMessage'>
            <h1>Oops! We were not able to retrieve the spaces. Please try again later.</h1>
          </div>
        }
        {/* If the data is successfully returned render the app's content */}
        { !this.state.hasError &&
          <Tabs
            tabs={spaceTabs}
            onSelect={(selected, selectedIndex) => {
              this.setState({
                selectedSpaceIndex: selectedIndex,
                selectedSpaceId: this.state.spaces[selectedIndex].sys.id
              })
            }}
            selected={this.state.selectedSpaceIndex}
          />
        }
      </div>
    )
  }
}

export default App