import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { Router } from "react-router"
import { createBrowserHistory } from "history"

const history = createBrowserHistory()

const AppWithRouter = (
    <Router history={history}>
        <App history={history} />
    </Router>
)

ReactDOM.render(AppWithRouter, document.getElementById('root'))

serviceWorker.unregister()
