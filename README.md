# Atlassian UI Coding Exercise

Steps to run the app:

1. Open a terminal window and clone the app into the location of your choice
2. Navigate to the root directory and run `npm install`
3. Navigate to the `app` directory and run `nvm use 8.10` followed by `npm install`
4. Navigate to the root directory and run `node server.js`
5. In a new terminal window or tab navigate to the `app` directory and run `npm run start`
6. Follow the steps in the terminal and navigate the app in the browser window

Steps to build a production version of the app:

1. Navigate to the `app` directory and run `npm run build`

Steps to run tests in the app:

1. Navigate to the `app` directory and run `npm run test`

**TODO: Write more unit tests to confirm expected functionality**

**TODO: Clean up console errors caused by misuse of the `<DynamicTable>` component**
